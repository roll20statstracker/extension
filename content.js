var campaignName = document.title.split('|')[0].trim();
var lastMessageId ="";
var rollWindowSelector = '#textchat > div.content';
var parsers = [new window.SWNRollParser(),new window.DefaultRollParser()];

console.log("Reporting data for "+ campaignName);


$("#textchat > div.content").bind('DOMSubtreeModified',function(){
  var rollElement = jQuery(rollWindowSelector + ' > div:nth-last-child(1)');
  var messageId = rollElement.data('messageid');
  if(lastMessageId != messageId){
    lastMessageId = messageId;
    for(var i =0; i <parsers.length;i++){
      if(parsers[i].canParseRoll(rollElement)){
        var roll = parsers[i].parseRollData(rollElement);
        console.log(roll);
        $.ajax({
          type:'POST',
          url:'https://localhost:9090/api/roll',
          data:{'roll':roll},
          success:function(data){
            console.log(data);
          },
        });
        break;
      }
    }
  }
});
