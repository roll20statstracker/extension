window.DefaultRollParser = function(){

}

window.DefaultRollParser.prototype ={
    canParseRoll: function(rollElement){
      return rollElement.hasClass('rollresult');
    },
    parseRollData: function(rollElement){
      var roll ={'campaign':campaignName};
      roll.messageId = rollElement.data('messageid');
      roll.time = moment().utc()._d;
      roll.player = this._getPlayer(rollElement,1);
      roll.result = rollElement.find('.rolled').text().trim();
      roll.formula = this._getFormula(rollElement);
      roll.formulaResult = this._getFormulaResult(rollElement);
      roll.type = RollType.DEFAULT;
      return roll;
    },
    _getPlayer : function(rollElement, distanceFromBotton){
      var playerNameElement = rollElement.find('.by');
      if(playerNameElement.length !== 0){
        return playerNameElement.text().split(':')[0];
      }
      else{
        var nextElementNumber = distanceFromBotton + 1;
          var elementAbove = jQuery(rollWindowSelector+ ' > div:nth-last-child('+nextElementNumber+')')
          return this._getPlayer(elementAbove,nextElementNumber);
        }
    },
    _getFormula: function(rollElement){
      return $(rollElement.find('.formula')[0]).text().trim().split(' ')[1].trim();
    },
    _getFormulaResult: function(rollElement){
      return $(rollElement.find('.formula')[1]).text().trim();
    }
}
