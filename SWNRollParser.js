window.SWNRollParser = function(){

}

window.SWNRollParser.prototype ={
    canParseRoll: function(rollElement){
      return rollElement.find('.sheet-swn').length > 0;
    },
    parseRollData: function(rollElement){
      var roll ={'campaign':campaignName};
      roll.messageId = rollElement.data('messageid');
      roll.time = moment().utc()._d;
      roll.player = this._getPlayer(rollElement,1);
      roll.result = $(rollElement.find('.inlinerollresult')[0]).text().trim();
      roll.formula = this._getFormula(rollElement);
      roll.formulaResult = this._getFormulaResult(rollElement);
      roll.type = this._getType(rollElement);
      return roll;
    },
    _getType: function(rollElement){
      var rollTitle = rollElement.find('.sheet-swn-template-title').text().trim();
      if(rollTitle.indexOf('Attacks') > 0)
      {
        return RollType.DAMAGE;
      }
      else if(rollTitle.indexOf('Skillcheck') > 0)
      {
        return RollType.SKILL_CHECK;
      }
      return RollType.DEFAULT;
    },
    _getPlayer : function(rollElement, distanceFromBotton){
      return rollElement.text().trim().split(' ')[0];
    },
    _getFormula: function(rollElement){
      return "";//$(rollElement.find('.formula')[0]).text().trim().split(' ')[1].trim();
    },
    _getFormulaResult: function(rollElement){
      return "";//$(rollElement.find('.formula')[1]).text().trim();
    }
}
